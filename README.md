# OSB Store

The OSB Store is a Single Page Application to connect and browse providers that implement the Open Service Broker API specification.

This application is built on [Vue.js](https://vuejs.org/) and aims to conform to the [official style guide](https://vuejs.org/v2/style-guide/) as closely as possible.

## Development

### SASS/CSS

When writing SASS/CSS the following principals should be remembered:

- Use [BEM Naming](http://getbem.com/naming/)
- [CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/) is supported
- [Bulma](https://bulma.io/) is installed and available

**Shared Styles**

Following the advice of a [CSS Tricks](https://css-tricks.com/how-to-import-a-sass-file-into-every-vue-component-in-an-app/) article we introduce some variables and mixins that are injected into every component.

This may cause issues if extracting components into their own individual repositories however this avoids excessive code duplication.

There is a long [Github issue](https://github.com/vuejs/vue-loader/issues/328) on this topic.

### Typescript

### State

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
