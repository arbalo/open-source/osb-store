import { KeycloakInstance } from 'keycloak-js';
import Keycloak from 'keycloak-js';
import Axios from 'axios';
import * as _ from 'lodash';

export const debug = process.env.VUE_APP_DEBUG ? process.env.VUE_APP_DEBUG.toLowerCase() === 'true' : false;

const keycloakConfig = {
    url: process.env.VUE_APP_KEYCLOAK_URL || 'localhost:8080',
    realm: process.env.VUE_APP_KEYCLOAK_REALM || 'arbalo',
    clientId: process.env.VUE_APP_KEYCLOAK_CLIENT_ID || 'arbalo',
};

export const keycloak: KeycloakInstance<'native'> = Keycloak<'native'>(keycloakConfig);

export function keycloakHeaderUpdate() {
    keycloak.updateToken(30).then(() => {
        setKeycloakHeaders(keycloak);
    });
}

export function setKeycloakHeaders(keycloak: KeycloakInstance<'native'>) {
    // Add an axios interceptor that adds the Authorization Header
    Axios.defaults.headers.common['Authorization'] = `Bearer ${keycloak.token}`;
    if (debug) {
        Axios.defaults.headers.common['X-Auth-Subject'] = keycloak.tokenParsed!.sub;
    }
}

export function isArbaloAdmin(): boolean {
    const roles = _.get(keycloak, 'tokenParsed.realm_access.roles', []) as string[];
    if (roles.length > 0 && roles.includes('arbalo:admin')) {
        return true;
    }
    return false;
}

