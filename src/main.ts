import Vue from 'vue';
import KeyCloak from 'keycloak-js';

import App from './App.vue';
import router from './router';
import store from '@/store';
import { keycloak, keycloakHeaderUpdate, setKeycloakHeaders } from './keycloak';

Vue.config.productionTip = false;

async function startApp() {

  keycloak.onTokenExpired = keycloakHeaderUpdate;
  keycloak.onAuthSuccess = keycloakHeaderUpdate;

  try {
    const success = await keycloak.init({
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri: `${window.location.origin}/check-sso.html`,
      promiseType: 'native',
    });

    if (success) {
      setKeycloakHeaders(keycloak);
    }
    Vue.prototype.$keycloak = keycloak;
    // After keycloak has initialised, then load app
    new Vue({
      router,
      store,
      render: (h) => h(App),
    }).$mount('#app');
  } catch (error) {
    throw new Error(`Unable to setup Keycloak: ${error}`);
  }
}

// this should be in it's own .d.ts file however it just refused to work
// https://vuejs.org/v2/guide/typescript.html#Augmenting-Types-for-Use-with-Plugins
declare module 'vue/types/vue' {
  interface Vue {
    $keycloak: KeyCloak.KeycloakInstance;
  }
}

startApp();
