import Vue from 'vue';
import Vuex, { StoreOptions, MutationTree, ActionTree, GetterTree } from 'vuex';

import { catalog } from './modules/catalog';
import { project } from './modules/project';
import { user } from './modules/user';
import { Message, MessageLevel } from './types';
import { Project } from '@/services/arbalo_extensions/projects';
import arbalo_extensions from '@/services/arbalo_extensions/service';

Vue.use(Vuex);

export interface RootState {
  serviceBrokerAPIBaseURL: string;
  serviceBrokerAPIVersion: string;
  loading: boolean;
  messages: Message[];
  projects: Project[];
  activeProject?: Project;
}

const state: RootState = {
  serviceBrokerAPIBaseURL:
    process.env.VUE_APP_OSBAPI_BASE_URL ? process.env.VUE_APP_OSBAPI_BASE_URL : 'default-api-base-url',
  serviceBrokerAPIVersion:
    process.env.VUE_APP_OSBAPI_VERSION ? process.env.VUE_APP_OSBAPI_VERSION : 'default-api-version',
  loading: false,
  messages: Array<Message>(),
  projects: Array<Project>(),
  activeProject: undefined,
};

const getters: GetterTree<RootState, RootState> = {
  getProjectByUUID(state: RootState, getters: any): Function {
    return (projectUUID: string) => {
      return state.projects.find((project: Project) => project.uuid === projectUUID);
    };
  },
};

const mutations: MutationTree<RootState> = {
  setMessage(state: RootState, message: Message) {
    state.messages.push(message);
  },
  setErrorMessage(state: RootState, message: string) {
    state.messages.push({ level: MessageLevel.Error, header: 'Error', body: message });
  },
  shiftMessage(state: RootState) {
    state.messages.shift();
  },
  setProjects(state: RootState, projects: Project[]) {
    state.projects = projects;
  },
  setActiveProject(state: RootState, project: Project) {
    state.activeProject = project;
  },
  setLoading(state: RootState) {
    state.loading = true;
  },
  unsetLoading(state: RootState) {
    state.loading = false;
  },
};

const actions: ActionTree<RootState, RootState> = {
  getMessage({ commit }: any): Message {
    return commit('shiftMessage');
  },
  async fetchProjects({ rootState, commit }) {
    try {
      const resp = await arbalo_extensions({ baseURL: rootState.serviceBrokerAPIBaseURL }).projects();

      if (resp.data && resp.data.length > 0) {
        commit('setProjects', resp.data);

        if (!rootState.activeProject) {
          commit('setActiveProject', resp.data[0]);
        }
      }
    } catch (error) {
      commit('setErrorMessage', 'Unable to load projects');
    }
  },
};

const store: StoreOptions<RootState> = {
  state,
  getters,
  actions,
  mutations,
  modules: {
    catalog,
    user,
    project,
  },
};

export default new Vuex.Store<RootState>(store);
