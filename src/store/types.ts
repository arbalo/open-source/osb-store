import { CatalogItem } from '@/services/osb/catalog';
import { KeycloakProfile } from 'keycloak-js';
import { ServiceInstance } from '@/services/arbalo_extensions/instances';
import { Profile } from '@/services/arbalo_extensions/profile';
import { Client } from '@/services/arbalo_extensions/clients';
import { ServiceUsageRecord } from '@/services/arbalo_extensions/usage';

export enum MessageLevel {
  Info = "info",
  Error = "error",
  Success = "success",
}

export const MessageLevelCSS: Map<MessageLevel, String> = new Map()
  .set(MessageLevel.Error, "is-danger")
  .set(MessageLevel.Success, "is-primary")
  .set(MessageLevel.Info, "is-info")

export interface Message {
  level: MessageLevel;
  header?: string;
  body?: string;
}

export interface ServiceState {
  services: CatalogItem[];
  instances: ServiceInstance[];
  expires: Date;
}

export interface UserState {
  keycloakProfile?: KeycloakProfile;
  arbaloProfile?: Profile;
}

export interface ActiveProjectState {
  clients?: Client[];
  usageRecords?: ServiceUsageRecord[];
}
