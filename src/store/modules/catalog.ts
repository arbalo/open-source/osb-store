import { CatalogItem } from '@/services/osb/catalog';
import osb from '@/services/osb/service';
import arbalo_extensions from '@/services/arbalo_extensions/service';
import { GetterTree, ActionTree, Module, MutationTree } from 'vuex';
import { ServiceState, MessageLevel } from '../types';
import { RootState } from '..';
import uuid from 'uuid/v4';
import _ from 'lodash';
import { OSBError, NewOSBError } from '@/services/osb/errors';
import { ServiceInstance } from '@/services/arbalo_extensions/instances';
import Fuse from 'fuse.js';

const messageServiceProvisioned = {
  level: MessageLevel.Success,
  header: 'Success',
  body: 'Service has been successfully provisioned',
};

const state: ServiceState = {
  services: Array<CatalogItem>(),
  instances: Array<ServiceInstance>(),
  expires: new Date(Date.now()),
};

const getters: GetterTree<ServiceState, RootState> = {
  getOfferingByID(state: ServiceState, getters: any): Function {
    return (id: string) => {
      return state.services.find((offering: CatalogItem) => offering.id === id);
    };
  },
  getInstancesForServiceUUID(state: ServiceState, getters: any): Function {
    return (uuid: string) => {
      return state.instances.find((instance: ServiceInstance) => instance.service_uuid === uuid);
    };
  },
  getServiceSetFromInstances(state: ServiceState, getters: any): Function {
    return () => {
      // Create a Set of instance IDs
      const serviceIDs = new Set();
      state.instances.forEach((item) => {
        serviceIDs.add(item.service_uuid);
      });
      return state.services.filter((item: CatalogItem) => serviceIDs.has(item.id));
    };
  },
  getServiceBySearch(state: ServiceState, getters: any): Function {
    const options: Fuse.FuseOptions<CatalogItem> = {
      shouldSort: true,
      maxPatternLength: 32,
      minMatchCharLength: 4,
      keys:
        [
          'name',
          'description',
          'tags',
          'metadata.abstract',
          'metadata.displayName',
          'metadata.longDescription',
          'metadata.providerDisplayName',
        ],
    };
    const fuse = new Fuse(state.services, options);
    return (searchString: string) => {
      return fuse.search(searchString);
    };
  },
};

const actions: ActionTree<ServiceState, RootState> = {
  async fetchCatalog({ rootState, commit }) {
    try {
      const resp = await osb({
        baseURL: rootState.serviceBrokerAPIBaseURL,
        apiVersion: rootState.serviceBrokerAPIVersion,
      }).catalog();
      commit('setServices', resp.services);
    } catch (err) {
      err = err as OSBError
      const message = err.humanReadable ? err.humanReadable : 'Unable to retrieve service catalog';
      this.commit('setErrorMessage', message);
    }
  },
  async provision({ rootState }, { serviceID, planID, projectUUID }) {
    try {
      const resp = await osb({
        baseURL: rootState.serviceBrokerAPIBaseURL,
        apiVersion: rootState.serviceBrokerAPIVersion,
      }).provision({
        instance_id: uuid(),
        service_id: serviceID,
        plan_id: planID,
        context: {
          project_uuid: projectUUID,
        },
      });
      // TODO - add this to array of known provisioned services?
      this.commit('setMessage', messageServiceProvisioned);
      if (resp === false) {
        return;
      } else if (typeof resp === 'object') {
        return resp;
      }
    } catch (err) {
      err = err as OSBError;
      const message = err.humanReadable ? err.humanReadable : 'Unable to provision service';
      this.commit('setErrorMessage', message);
    }
  },
  async fetchInstances({ rootState, commit }, { projectUUID }) {
    const resp = await arbalo_extensions({ baseURL: rootState.serviceBrokerAPIBaseURL }).instances(projectUUID);
    commit('setInstances', resp.data);
  },
  async deprovision({ rootState }, { instanceID, serviceID, planID }) {
    try {
      const resp = await osb({
        baseURL: rootState.serviceBrokerAPIBaseURL,
        apiVersion: rootState.serviceBrokerAPIVersion,
      }).deprovision({
        instance_id: instanceID,
        service_id: serviceID,
        plan_id: planID,
      });
      return resp;
    } catch (err) {
      err = err as OSBError;
      const message = err.humanReadable ? err.humanReadable : 'Unable to deprovision service';
      this.commit('setErrorMessage', message);
    }
  },
};

const mutations: MutationTree<ServiceState> = {
  setServices(state: ServiceState, services: CatalogItem[]) {
    state.services = services;
    state.expires = addHours(
      new Date(Date.now()),
      Number(_.get(process.env, 'VUE_APP_VUEX_CATALOG_EXPIRE_HOURS', 24)),
    );
  },
  setInstances(state: ServiceState, instances: ServiceInstance[]) {
    state.instances = instances;
  },
};

export const catalog: Module<ServiceState, RootState> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

function addHours(date: Date, hours: number): Date {
  date.setTime(date.getTime() + (1000 * 60 * 60 * hours));
  return date;
}
