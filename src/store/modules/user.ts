import { Module, GetterTree, ActionTree, MutationTree } from "vuex";
import { UserState } from '../types';
import { RootState } from '..';
import { KeycloakInstance, KeycloakProfile } from 'keycloak-js';
import arbalo_extensions from '@/services/arbalo_extensions/service';
import { Profile } from '@/services/arbalo_extensions/profile';

const state: UserState = {
  keycloakProfile: undefined,
  arbaloProfile: undefined,
};

const getters: GetterTree<UserState, RootState> = {
  getArbaloProfile(state: UserState, getters: any): Function {
    return () => {
      return state.arbaloProfile
    };
  },
};

const actions: ActionTree<UserState, RootState> = {
  fetchKeycloakProfile({ rootState, commit }, keycloak_instance: KeycloakInstance<'native'>) {
    keycloak_instance.loadUserProfile().then(function (profile) {
      commit('setKeycloakProfile', profile);
    });
    keycloak_instance.loadUserInfo
  },
  async fetchArbaloProfile({ rootState, commit }, { profileUUID }) {
    if (!profileUUID) {
      console.log('profile uuid undefined');
      return;
    }
    const resp = await arbalo_extensions({ baseURL: rootState.serviceBrokerAPIBaseURL }).profile(profileUUID);
    commit("setArbaloProfile", resp);
  },
};

const mutations: MutationTree<UserState> = {
  setKeycloakProfile(state: UserState, profile: KeycloakProfile) {
    state.keycloakProfile = profile;
  },
  setArbaloProfile(state: UserState, profile: Profile) {
    state.arbaloProfile = profile;
  },
};

export const user: Module<UserState, RootState> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
