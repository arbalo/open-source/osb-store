import { GetterTree, ActionTree, MutationTree, Module } from "vuex";
import { RootState } from '..';
import { ActiveProjectState, MessageLevel } from '../types';
import { Client } from '@/services/arbalo_extensions/clients';
import arbalo_extensions from '@/services/arbalo_extensions/service';
import { ServiceUsageRecord } from '@/services/arbalo_extensions/usage';
import moment from 'moment';
import Vue from 'vue';

const state: ActiveProjectState = {
    clients: undefined,
    usageRecords: undefined,
};

const getters: GetterTree<ActiveProjectState, RootState> = {
    getUsageDates(state, getters): string[] {
        if (state.usageRecords) {
            return state.usageRecords.map((usage) => moment(usage.date).format('DD-MM'));
        }
        return Array<string>();
    },
    getUsageValues(state, getters): number[] {
        if (state.usageRecords) {
            return state.usageRecords.map((usage) => usage.service_counts);
        }
        return Array<number>();
    },
};

const actions: ActionTree<ActiveProjectState, RootState> = {
    async fetchClientsForActiveProject({ rootState, commit }) {
        if (!rootState.activeProject) {
            console.log('No active project so not loading clients');
            return;
        }

        const resp = await arbalo_extensions(
            { baseURL: rootState.serviceBrokerAPIBaseURL },
        ).clients(rootState.activeProject.uuid);

        commit('setClients', resp.data);
    },
    async createProject({ rootState, commit, dispatch }, { project }) {
        // Create a new project
        try {
            const resp = await arbalo_extensions(
                { baseURL: rootState.serviceBrokerAPIBaseURL },
            ).createProject(project);
            dispatch('setActiveProject', resp);
            rootState.projects.push(resp);
            commit('setMessage', {
                level: MessageLevel.Success,
                header: 'Success',
                body: `Project "${project.name}" created successfully!`,
            });
        } catch (error) {
            console.error(`Unable to create project: ${error}`);
            commit('setErrorMessage', 'Unable to create new project');
        }
    },
    async fetchUsageForActiveProject({ rootState, commit }, { numberOfDays }) {
        if (!rootState.activeProject) {
            console.log('No active project so not loading usage');
            return;
        }
        const resp = await arbalo_extensions(
            { baseURL: rootState.serviceBrokerAPIBaseURL },
        ).usage(rootState.activeProject.uuid, new Date(), numberOfDays);
        commit('setUsage', resp.records);
    },
    async pollLatestUsage({ rootState, commit }) {
        if (!rootState.activeProject) {
            console.log('No active project so not loading usage');
            return;
        }

        let es = await arbalo_extensions(
            { baseURL: rootState.serviceBrokerAPIBaseURL },
        ).pollLatestUsage(rootState.activeProject.uuid);
        es.onmessage = (message) => {
            commit('updateLatestUsage',
                { date: moment().format('DD-MM'), service_counts: message.data } as ServiceUsageRecord);
        };
    },
    // Creates a new client to the backend and upon success retrieves the updated list
    async newClient({ rootState, dispatch }) {
        if (!rootState.activeProject) {
            console.log("No active project so not creating new client");
            return;
        }
        const resp = await arbalo_extensions(
            { baseURL: rootState.serviceBrokerAPIBaseURL }
        ).newClient(rootState.activeProject.uuid);

        dispatch('fetchClientsForActiveProject');
    },
    async disableClient({ rootState, commit }, client) {
        if (!rootState.activeProject) {
            return;
        }

        try {
            const resp = await arbalo_extensions(
                { baseURL: rootState.serviceBrokerAPIBaseURL },
            ).removeClient(rootState.activeProject.uuid, client.uuid);
        } catch (error) {
            console.error(error);
        }

        commit('removeClient', client);
    },
};

const mutations: MutationTree<ActiveProjectState> = {
    setClients(state: ActiveProjectState, clients: Client[]) {
        state.clients = clients;
    },
    setUsage(state: ActiveProjectState, usage: ServiceUsageRecord[]) {
        state.usageRecords = usage;
    },
    updateLatestUsage(state: ActiveProjectState, usage: ServiceUsageRecord) {
        if (!state.usageRecords || state.usageRecords.length === 0) {
            return;
        }
        state.usageRecords.splice(-1, 1, usage);
    },
    removeClient(state: ActiveProjectState, client: Client) {
        if (!state.clients || state.clients.length === 0) {
            return;
        }
        const clientIdx = state.clients.findIndex((c: Client) => {
            return client.uuid === c.uuid;
        });
        if (clientIdx < 0) {
            console.log(`No clients found in state for ${client}`);
            return;
        }
        state.clients.splice(clientIdx, 1);
    },
};

export const project: Module<ActiveProjectState, RootState> = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
