import Vue from 'vue';
import Router, { Route } from 'vue-router';
import Dashboard from '@/views/Dashboard.vue';
import DashboardAddProject from '@/components/DashboardAddProject.vue';
import ServiceCatalog from '@/views/ServiceCatalog.vue';
import ServiceInstanceOverview from '@/views/ServiceInstanceOverview.vue';
import ServiceOffering from '@/views/ServiceOffering.vue';
import ProjectDashboard from '@/views/ProjectDashboard.vue';
import ServiceOfferingDashboard from '@/components/ServiceOfferingDashboard.vue';
import ServiceOfferingPlan from '@/components/ServiceOfferingPlan.vue';
import ServiceOfferingReleaseNotes from '@/components/ServiceOfferingReleaseNotes.vue';
import ServiceOfferingReview from '@/components/ServiceOfferingReview.vue';
import AddServiceProvider from './views/AddServiceProvider.vue';
import { keycloak, isArbaloAdmin } from './keycloak';

function authGuard(to: Route, from: Route, next: Function) {
  if (keycloak.authenticated) {
    next();
  } else {
    keycloak.login();
  }
}

function arbaloAdminAuthGuard(to: Route, from: Route, next: Function) {
  if (!keycloak.authenticated) {
    keycloak.login();
  } else {
    if (isArbaloAdmin()) {
      console.log('Welcome Arbalo Admin');
      next();
    } else {
      next({ name: 'error-unauthorized' });
    }
  }
}

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: authGuard,
      children: [
        {
          path: 'project/new',
          name: 'dashboard-add_project',
          component: DashboardAddProject,
        },
        {
          path: 'project/:projectUUID',
          name: 'dashboard-project',
          component: ProjectDashboard,
          props: true,
          children: [
            {
              path: 'settings',
              name: 'dashboard-project-settings',
              component: ProjectDashboard,
            },
          ],
        },
      ],
    },
    {
      path: '/instance/:id',
      name: 'instance-overview',
      component: ServiceInstanceOverview,
    },
    {
      path: '/service/catalog',
      name: 'service-catalog',
      component: ServiceCatalog,
    },
    {
      path: '/service/register',
      name: 'service-register',
      component: AddServiceProvider,
      beforeEnter: arbaloAdminAuthGuard,
    },
    {
      path: '/service/:id',
      name: 'service-overview',
      component: ServiceOffering,
      redirect: { name: 'service_offering-dashboard' },
      children: [
        {
          path: 'dashboard',
          name: 'service_offering-dashboard',
          component: ServiceOfferingDashboard,
        },
        {
          path: 'add-service',
          name: 'add-service',
          component: AddServiceProvider,
        },
        {
          path: 'plans',
          name: 'service_offering-plans',
          component: ServiceOfferingPlan,
        },
        {
          path: 'release-notes',
          name: 'service_offering-release',
          component: ServiceOfferingReleaseNotes,
        },
        {
          path: 'reviews',
          name: 'service_offering-reviews',
          component: ServiceOfferingReview,
        },
        {
          path: 'instances',
          name: 'service_offering-instance',
          component: ServiceOfferingReview,
        },
      ],
    },
    {
      path: 'embed',
      children: [
        {
          path: 'service/catalog',
          component: ServiceCatalog,
        },
      ],
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/error',
      name: 'error',
      children: [
        {
          path: 'unauthorized',
          name: 'error-unauthorized',
        },
      ],
    },
  ],
});
