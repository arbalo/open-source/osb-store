import axios, { AxiosRequestConfig, AxiosError, AxiosResponse, CancelTokenSource, AxiosInstance } from 'axios';

export interface PollOptions<PollResponse, CompleteResponse> {
    interval: number;
    shouldContinuePolling: (response: PollResponse | CompleteResponse) => response is PollResponse;
    isComplete: (response: PollResponse | CompleteResponse) => response is CompleteResponse;
    onStart?: () => any;
    onStop?: () => any;
    onError: (error: AxiosError) => any;
    onComplete: (response: CompleteResponse) => any;
}

export class Poller<PollResponse, CompleteResponse> {
    private axiosInstance: AxiosInstance;
    private axiosConfig: AxiosRequestConfig;
    private options: PollOptions<PollResponse, CompleteResponse>;
    private polling: boolean = false;
    private timeout?: number;
    private cancelTokenSource?: CancelTokenSource;

    public constructor(axiosInstance: AxiosInstance, config: AxiosRequestConfig, options: PollOptions<PollResponse, CompleteResponse>) {
        this.axiosInstance = axiosInstance;
        this.axiosConfig = config;
        this.options = options;
    }

    public start() {
        if (!this.polling) {
            this.polling = true;

            this.startPoll();

            if (typeof this.options.onStart === 'function') {
                this.options.onStart();
            }
        }
    }

    public stop() {
        if (this.polling) {
            this.polling = false;
            window.clearTimeout(this.timeout);

            if (this.cancelTokenSource) {
                this.cancelTokenSource.cancel();
            }

            if (typeof this.options.onStop === 'function') {
                this.options.onStop();
            }
        }
    }

    private startPoll() {
        this.cancelTokenSource = axios.CancelToken.source();

        this.axiosInstance
            .request({
                ...this.axiosConfig,
                cancelToken: this.cancelTokenSource.token,
            })
            .then(
                (response: AxiosResponse) => {
                    if (this.options.shouldContinuePolling(response.data)) {
                        this.timeout = window.setTimeout(() => {
                            this.startPoll();
                        }, this.options.interval);
                    } else if (this.options.isComplete(response.data)) {
                        this.options.onComplete(response.data);
                    } else {
                        throw new Error('Polling was not complete, and should not continue');
                    }
                },
                (error: AxiosError) => {
                    if (axios.isCancel(error)) {
                        console.error('Polling cancelled');
                    } else {
                        this.options.onError(error);
                    }
                }
            );
    }
}

export function NewPoller(
    instance: AxiosInstance, config: AxiosRequestConfig, options: PollOptions<any, any>): Poller<any, any> {
    return new Poller(instance, config, options);
}

export default NewPoller;
