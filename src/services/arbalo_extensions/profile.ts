import { AxiosInstance } from 'axios';
import { NewArbaloExtensionsError } from './errors';
import { ErrorUnexpectedResponseCode } from '../osb/errors';

const endpointProfile = 'v1/profile';

export interface Profile {
    id?: number;
    uuid?: string;
    registration_completed?: Date;
    avatar_url?: string;
}

export interface RegistrationCompletion {
    terms_and_conditions_accepted: boolean;
}

export async function GetProfile(client: AxiosInstance, profileUUID: string): Promise<Profile> {
    const resp = await client.get(`${endpointProfile}/${profileUUID}`);
    switch (resp.status) {
        case 200: {
            return resp.data;
        }
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}

export async function CompleteRegistration(client: AxiosInstance, profileUUI: string, payload: RegistrationCompletion) {
    const resp = await client.post(`${endpointProfile}/${profileUUI}/complete-registration`, payload);
    switch (resp.status) {
        case 200:
            return resp.data;
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}
