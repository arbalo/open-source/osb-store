/* *** Error definitions *** */
export const ErrorUnexpectedResponseCode = (responseCode: number) => {
  return new Error(`Unexpected status code received when retrieving calling Arbalo Extensions API: ${responseCode}`);
};

export const ErrorInvalidProjectUUID = (projectUUID: string) => {
  return new Error(`Invalid Project UUID: ${projectUUID}`);
};

/* *** Custom interface to throw *** */
export interface ArbaloExtensionsError {
  error: Error;
  serviceResponse?: string;
  humanReadable?: string;
}

export function NewArbaloExtensionsError(
  error: Error, serviceResponse?: string, humanReadable?: string,
): ArbaloExtensionsError {
  return { error, serviceResponse, humanReadable };
}
