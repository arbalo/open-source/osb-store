import { AxiosInstance } from 'axios';
import { NewArbaloExtensionsError } from './errors';
import { ErrorUnexpectedResponseCode } from '../osb/errors';

const endpointClientForProject = (projectUUID: string) => `v1/project/${projectUUID}/client`;
const endpointClientDetailsForProject = (projectUUID: string, clientID: string) => {
    return `${endpointClientForProject(projectUUID)}/${clientID}`;
};

export interface Client {
    id: string;
    uuid: string;
    service_account: string;
    secret: string;
}

export interface ClientResponse {
    data: Client[];
}

export async function GetClients(client: AxiosInstance, projectUUID: string): Promise<ClientResponse> {
    const resp = await client.get(endpointClientForProject(projectUUID));
    switch (resp.status) {
        case 200: {
            return resp.data;
        }
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}

export async function NewClient(client: AxiosInstance, projectUUID: string): Promise<Client> {
    const resp = await client.post(endpointClientForProject(projectUUID));
    switch (resp.status) {
        case 201: {
            return resp.data;
        }
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}

export async function RemoveClient(client: AxiosInstance, projectUUID: string, clientID: string): Promise<void> {
    const resp = await client.delete(endpointClientDetailsForProject(projectUUID, clientID));
    switch (resp.status) {
        case 200: {
            return;
        }
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}
