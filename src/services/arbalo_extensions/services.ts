import { AxiosInstance } from 'axios';

const endpointRegisterService = `v1/service`;

export interface PricePlanRequest {
    displayName: string;
    description: string;
    free: boolean;
    features: string[];
    costs: {
        unit: string;
        amount: Array<{ [key: string]: string }>;
    };
}

export function EmptyPricePlanRequest(): PricePlanRequest {
    return { displayName: '', description: '', free: false, features: [], costs: { unit: '', amount: [] } };
}

export interface ServiceRequest {
    name: string;
    abstract: string;
    description: string;
    graphic: string;
    licenseId: number;
    lifecycleStage: string;
    upstream: string;
    credentials_type: string;
    credentials: any;
    price_plans: PricePlanRequest[];
}

export async function RegisterService(client: AxiosInstance, request: ServiceRequest): Promise<Error | void> {
    const resp = await client.post(endpointRegisterService, request);
    switch (resp.status) {
        case 201: {
            return;
        }
        default: {
            return {
                name: '',
                message: '',
            };
        }
    }
}
