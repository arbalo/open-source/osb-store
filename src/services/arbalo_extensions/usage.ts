import { AxiosInstance } from 'axios';
import poller, { PollOptions } from '@/services/poller';
import { NewArbaloExtensionsError } from './errors';
import { ErrorUnexpectedResponseCode } from '../osb/errors';

const endpointUsage = (projectUUID: string) => `v1/project/${projectUUID}/usage`;
const endpointLatestUsage = (baseURL: string, projectUUID: string) => `${baseURL}/v1/project/${projectUUID}/usage/live`;

export async function GetUsage(client: AxiosInstance, projectUUID: string, start_date: Date, number_of_days: number): Promise<ServiceUsageResponse> {
  const resp = await client.get(endpointUsage(projectUUID), {
    params: {
      days: number_of_days,
    },
  });
  switch (resp.status) {
    case 200: {
      return resp.data;
    }
    default: {
      throw NewArbaloExtensionsError(ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description);
    }
  }
}

export async function PollLatestUsage(
  baseURL: string,
  projectUUID: string,
) {
  return new EventSource(endpointLatestUsage(baseURL, projectUUID));
}

export interface ServiceUsageRecord {
  date: string;
  service_counts: number;
}

export interface ServiceUsageResponse {
  name: string;
  records: ServiceUsageRecord[];
}
