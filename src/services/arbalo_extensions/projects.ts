import { AxiosInstance } from 'axios';
import { NewArbaloExtensionsError } from './errors';
import { ErrorUnexpectedResponseCode } from '../osb/errors';

const endpointProjects = "v1/project";

export interface Project {
    id: number;
    uuid: string;
    title: string;
    slug: string;
}

export interface ProjectResponse {
    data: Project[];
}

export async function CreateProject(client: AxiosInstance, project: Project): Promise<Project> {
    const resp = await client.post(`${endpointProjects}`, project);
    switch (resp.status) {
        case 201: {
            return resp.data;
        }
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}

export async function GetProjects(client: AxiosInstance): Promise<ProjectResponse> {
    const resp = await client.get(`${endpointProjects}`);
    switch (resp.status) {
        case 200: {
            return resp.data;
        }
        default: {
            throw NewArbaloExtensionsError(
                ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description,
            );
        }
    }
}
