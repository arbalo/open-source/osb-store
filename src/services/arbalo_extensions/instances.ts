import { AxiosInstance } from 'axios';
import { ErrorUnexpectedResponseCode, NewArbaloExtensionsError, ErrorInvalidProjectUUID } from './errors';

const endpointInstances = (projectUUID: string) => `v1/project/${projectUUID}/instance`;

export async function GetInstances(client: AxiosInstance, projectUUID: string): Promise<ServiceInstancesResponse> {
  if (!projectUUID || projectUUID === '') {
    throw NewArbaloExtensionsError(ErrorInvalidProjectUUID(projectUUID));
  }

  const resp = await client.get(endpointInstances(projectUUID));
  switch (resp.status) {
    case 200: {
      return resp.data;
    }
    default: {
      throw NewArbaloExtensionsError(ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description);
    }
  }
}

export interface ServiceInstancesResponse {
  data: ServiceInstance[];
}

export interface ServiceInstanceParameters {

}

export interface ServiceInstance {
  dashboard_url: string;
  id: number;
  uuid: string;
  parameters: ServiceInstanceParameters;
  plan_id: number;
  plan_uuid: string;
  project_id: number;
  service_uuid: string;
  subscription_start_time: Date;
  subscription_end_time: Date;
}

export function emptyInstance(): ServiceInstance {
  return {
    dashboard_url: '',
    id: 0,
    uuid: '',
    parameters: {},
    plan_id: 0,
    plan_uuid: '',
    project_id: 0,
    service_uuid: '',
    subscription_start_time: new Date(),
    subscription_end_time: new Date(),
  };
}
