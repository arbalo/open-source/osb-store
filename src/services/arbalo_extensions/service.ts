import { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import axios from 'axios';
import { GetInstances } from './instances';
import { GetProfile, CompleteRegistration, RegistrationCompletion } from './profile';
import { GetUsage, PollLatestUsage, ServiceUsageRecord } from './usage';
import { GetProjects, Project, CreateProject } from './projects';
import { GetClients, NewClient, RemoveClient } from './clients';
import { RegisterService, ServiceRequest } from './services';

export class ArbaloExtensions {
  public client: AxiosInstance;
  public baseURL: string;

  constructor(axiosConfig: AxiosRequestConfig) {
    this.client = axios.create(axiosConfig);
    this.baseURL = axiosConfig.baseURL as string;
  }

  public profile(profileUuid: string) {
    return GetProfile(this.client, profileUuid);
  }

  public completeRegistration(profileUuid: string, payload: RegistrationCompletion) {
    return CompleteRegistration(this.client, profileUuid, payload);
  }

  public projects() {
    return GetProjects(this.client);
  }

  public createProject(project: Project) {
    return CreateProject(this.client, project);
  }

  public instances(projectId: string) {
    return GetInstances(this.client, projectId);
  }

  public usage(projectUUID: string, startDate: Date, numberOfDays: number) {
    return GetUsage(this.client, projectUUID, startDate, numberOfDays);
  }

  public clients(projectUUID: string) {
    return GetClients(this.client, projectUUID);
  }

  public newClient(projectUUID: string) {
    return NewClient(this.client, projectUUID);
  }

  public removeClient(projectUUID: string, clientID: string) {
    return RemoveClient(this.client, projectUUID, clientID);
  }

  public pollLatestUsage(projectUUID: string) {
    return PollLatestUsage(this.baseURL, projectUUID);
  }

  public insert(service: ServiceRequest) {
    return RegisterService(this.client, service);
  }
}

export interface ArbaloExtensionsConnectionConfig {
  baseURL: string;
}

export function NewArbaloExtensionsService(config: ArbaloExtensionsConnectionConfig): ArbaloExtensions {
  let baseURL = config.baseURL;
  if (!baseURL.endsWith('/')) {
    baseURL += '/';
  }
  return new ArbaloExtensions({ baseURL, timeout: 100000 });
}

export default NewArbaloExtensionsService;
