import { AxiosRequestConfig, AxiosInstance } from 'axios';
import axios from 'axios';

import { GetCatalog } from './catalog';
import { Provision, OSBProvisionParams, Deprovision, OSBDeprovisionParams } from './instance';

const endpointLastOperationServiceInstances =
  (instanceID: string) => {
    return `v2/service_instances/${instanceID}/last_operation`;
  };
const endpointLastOperationServiceBinding =
  (instanceID: string, bindingID: string) => {
    return `v2/service_instances/${instanceID}/service_bindings/${bindingID}/last_operation`;
  };

export interface OSBLastOperationParams {
  instance_id: string;
  service_id?: string;
  plan_id?: string;
  operation?: string;
}

export interface OSBLastOperationBindingParams extends OSBLastOperationParams {
  binding_id: string;
}

export class ServiceBroker {
  public client: AxiosInstance;

  constructor(axiosConfig: AxiosRequestConfig) {
    this.client = axios.create(axiosConfig);
  }

  public catalog() {
    return GetCatalog(this.client);
  }

  public async provision(params: OSBProvisionParams) {
    return Provision(this.client, params);
  }

  public poll_instance() { }

  public async last_operation_service_instance(params: OSBLastOperationParams) {
    this.client.get(
      endpointLastOperationServiceInstances(params.instance_id),
      { params: extractQueryParameters(params) },
    );
  }

  public async last_operation_service_binding(params: OSBLastOperationBindingParams) {
    this.client.get(
      endpointLastOperationServiceBinding(params.instance_id, params.binding_id),
      { params: extractQueryParameters(params) },
    );
  }

  public async fetch_service_instance() {
    return
  }
  public async update_service() { }
  public async bind() { }
  public async unbind() { }

  public async deprovision(params: OSBDeprovisionParams) {
    return Deprovision(this.client, params);
  }
}

// OSBConnectionConfig defines the basic configuration to an implementation
// of the OSB API
export interface OSBConnectionConfig {
  baseURL: string;
  apiVersion: string;
  originatingIdentity?: string;
}

export function NewOSBService(config: OSBConnectionConfig): ServiceBroker {
  const headers: any = { 'X-Broker-API-Version': config.apiVersion };
  if (config.originatingIdentity) {
    headers['X-Broker-API-Originating-Identity'] = config.originatingIdentity;
  }
  // Append a `/` if required
  let baseURL = config.baseURL;
  if (!baseURL.endsWith('/')) {
    baseURL += '/';
  }
  return new ServiceBroker({
    baseURL,
    headers,
    timeout: 100000,
  });
}

function extractQueryParameters(params: OSBLastOperationParams): any {
  const queryParams: any = {};
  if (params.service_id) {
    queryParams.service_id = params.service_id;
  }
  if (params.plan_id) {
    queryParams.plan_id = params.plan_id;
  }
  if (params.operation) {
    queryParams.operation = params.operation;
  }
  return queryParams;
}

export default NewOSBService;
