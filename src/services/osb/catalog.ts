import { AxiosInstance } from 'axios';
import { ErrorUnexpectedResponseCode, NewOSBError } from './errors';

const endpointCatalog = 'v2/catalog';

export async function GetCatalog(client: AxiosInstance): Promise<ServiceCatalogResponse> {
  const resp = await client.get(`${endpointCatalog}`);
  switch (resp.status) {
    case 200: {
      return resp.data;
    }
    default: {
      throw NewOSBError(ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description)
    }
  }
}

export interface ServiceCatalogResponse {
  services: CatalogItem[];
}

// CatalogItemMetadata implements the RECOMMENDED fields.
// There may of course be more than these fields but it is up to the
// client library to check if they are present and act accordingly.
// More information: https://github.com/openservicebrokerapi/servicebroker/blob/master/profile.md#service-metadata
export interface CatalogItemMetadata {
  displayName?: string;
  imageUrl?: string;
  licenseUrl?: string;
  longDescription?: string;
  providerDisplayName?: string;
  documentationUrl?: string;
  supportUrl?: string;
  swaggerUrl?: string;
}

export interface CatalogItem {
  name: string;
  id: string;
  description: string;
  tags?: string[];
  requires?: string[];
  bindable: boolean;
  instances_retrievable?: boolean;
  bindings_retrievable?: boolean;
  allow_context_updates?: boolean;
  metadata?: CatalogItemMetadata;
  dashboard_client?: DashboardClient;
  plan_updateable?: boolean;
  plans: ServicePlan[];
}

export function emptyCatalogItem(): CatalogItem {
  return {
    name: '',
    id: '',
    description: '',
    bindable: false,
    plans: [],
  };
}

export interface DashboardClient {
  id: string;
  secret: string;
  redirect_uri?: string;
}

export interface ServicePlanCost {
  amount: any;
  unit: string;
}

// ServicePlanMetadata implements the RECOMMENDED fields.
// More information: https://github.com/openservicebrokerapi/servicebroker/blob/master/profile.md#plan-metadata-fields
export interface ServicePlanMetadata {
  bullets?: string[];
  costs?: ServicePlanCost[];
  displayName?: string;
}

export interface ServicePlan {
  id: string;
  name: string;
  description: string;
  metadata?: ServicePlanMetadata;
  free?: boolean;
  bindable?: boolean;
  plan_updateable?: boolean;
  schemas?: Schemas;
  maximum_polling_duration?: number;
}

export interface Schemas {
  service_instance?: ServiceInstanceSchema;
  service_binding?: ServiceBindingSchema;
}

export interface ServiceInstanceSchema {
  create?: InputParametersSchema;
  update?: InputParametersSchema;
}

export interface ServiceBindingSchema {
  create?: InputParametersSchema;
}

export interface InputParametersSchema {
  parameters?: any;
}
