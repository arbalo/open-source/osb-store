export interface ErrorResponse {
  error?: string;
  description?: string;
}

export interface FetchServiceResponse {
  service_id?: string;
  plan_id?: string;
  dashboard_url?: string;
  parameters?: any;
}

export interface ProvisionResponse {
  dashboard_url?: string;
  operation?: string;
}

export interface UpdateResponse extends ProvisionResponse { }

export enum State {
  inProgress = 'in progress',
  succeeded = 'succeeded',
  failed = 'failed',
}

export interface LastOperationResponse {
  state: State;
  description?: string;
}

export interface CreateBindingResponseAccepted {
  operation?: string;
}

export interface Device {
  volume_id: string;
  mount_config?: any;
}

export interface VolumeMount {
  driver: string;
  container_dir: string;
  mode: string;
  device_type: string;
  device: Device;
}

export interface CreateBindingResponseOKCreated {
  credentials?: string;
  syslog_drain_url?: string;
  route_service_url?: string;
  volume_mounts?: VolumeMount[];
}

export interface FetchBindingResponse {
  credentials?: any;
  syslog_drain_url?: string;
  route_service_url?: string;
  volume_mounts?: VolumeMount[];
  parameters?: any;
}

export interface UnbindResponse {
  operation?: string;
}

export interface DeprovisionResponse {
  operation?: string;
}
