import { AxiosInstance } from 'axios';
import {
  ErrorAsyncProvisioningRequired, ErrorUnexpectedResponseCode, ErrorInstanceDoesNotExist,
  ErrorBadRequest, ErrorConflictService, NewOSBError
} from './errors';

const endpointService =
  (instanceID: string) => { return `v2/service_instances/${instanceID}` };

export interface OSBProvisionParams {
  instance_id: string;
  service_id: string;
  plan_id: string;
  accepts_incomplete?: boolean;
  organization_guid?: string;
  context?: any;
  space_guid?: string;
  parameters?: any;
}

export interface ProvisionResponse {
  dashboard_url?: string;
  operation?: string;
}

// Provision will return a response or a boolean value of false if the service is not ready yet
export async function Provision(
  client: AxiosInstance, params: OSBProvisionParams,
): Promise<ProvisionResponse | boolean> {
  const resp = await client.put(
    endpointService(params.instance_id),
    params,
  );
  switch (resp.status) {
    // More info on response codes
    // https://github.com/openservicebrokerapi/servicebroker/blob/master/spec.md#response-3
    case 200: {
      return resp.data;
    }
    case 201: {
      return resp.data;
    }
    case 202: {
      // If service is still provisioning return a false value
      return false;
    }
    case 400: {
      throw NewOSBError(ErrorBadRequest, resp.data.error, resp.data.description);
    }
    case 409: {
      throw NewOSBError(ErrorConflictService(params.instance_id), resp.data.error, resp.data.description);
    }
    case 422: {
      throw NewOSBError(ErrorAsyncProvisioningRequired, resp.data.error, resp.data.description)
    }
    default: {
      throw NewOSBError(ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description);
    }
  }
}

export interface OSBDeprovisionParams {
  instance_id: string;
  service_id: string;
  plan_id: string;
  accepts_incomplete?: boolean;
}

export interface OSBDeprovisionResponse {
  operation?: string;
}

export async function Deprovision(
  client: AxiosInstance, params: OSBProvisionParams,
) {
  const resp = await client.delete(
    endpointService(params.instance_id),
    { params: params },
  );
  switch (resp.status) {
    case 200: {
      return resp.data;
    }
    case 201: {
      return resp.data;
    }
    case 400: {
      throw NewOSBError(ErrorBadRequest, resp.data.error, resp.data.description);
    }
    case 410: {
      throw NewOSBError(ErrorInstanceDoesNotExist(params.instance_id), resp.data.error, resp.data.description);
    }
    case 422: {
      throw NewOSBError(ErrorAsyncProvisioningRequired, resp.data.error, resp.data.description)
    }
    default: {
      throw NewOSBError(ErrorUnexpectedResponseCode(resp.status), resp.data.error, resp.data.description);
    }
  }
}
