/* *** Error definitions *** */
export const ErrorUnexpectedResponseCode = (responseCode: number) => {
  return new Error(`Unexpected status code received when retrieving catalog: ${responseCode}`);
};

export const ErrorConflictService = (instanceID: string) => {
  return new Error(`An instance already exists or is being processed with ID: ${instanceID}`);
};

export const ErrorBadRequest = new Error(`Malformed request or data missing`);
export const ErrorAsyncProvisioningRequired = new Error(
  `Service instance must be provisioned with asynchronus provisioning`);

export const ErrorInstanceDoesNotExist = (instanceID: string) => {
  return new Error(`No instance found with ID: ${instanceID}`);
};

/* *** Matching error response for OSB Errors *** */
export interface OSBErrorResponse {
  error?: string;
  description?: string;
}

/* *** Custom interface to throw *** */
export interface OSBError {
  error: Error
  serviceResponse?: string
  humanReadable?: string
}

export function NewOSBError(error: Error, serviceResponse?: string, humanReadable?: string): OSBError {
  return { error, serviceResponse, humanReadable }
}

