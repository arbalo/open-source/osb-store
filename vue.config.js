module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/styles/_variables.scss";\n        @import "@/styles/_mixins.scss";'
      }
    }
  },
  productionSourceMap: false,
};
